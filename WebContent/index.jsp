<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Stock Management App</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style>
html, body {
	height: 100%;
	font-size: 1.2em;
}


.form-signin {
	width: 100%;
	max-width: 530px;
	padding: 15px;
	margin: auto;
}



.form-signin .form-control {
	position: relative;
	box-sizing: border-box;
	height: auto;
	padding: 10px;
	font-size: 16px;
}

.form-signin .form-control:focus {
	z-index: 2;
}

input.form-control::placeholder{
	font-size: 1.1em;
}

</style>
</head>
<body class="text-center">
	


	<form style="position: relative; top: 200px;" class="form-signin" action="LoginServlet"  method="post">
		
		<h1 class="h3 mb-3 font-weight-normal">Welcome</h1>
		<h5 class="h5 mb-4 font-weight-normal">Please login to access the Stock Management Application</h5>
		<label for="access-name" class="sr-only">Access name</label> 
		<input  class="form-control" id="access-name" name="access-name" placeholder="Access Name">
		<label for="password" class="sr-only">Password</label> 
		<input  class="form-control" id="password" type="password" name="password" placeholder="Password">
		<%
		if (request.getParameter("error") != null) {
		%>
		<div style="color: red; font-size: 0.8em;">
		Incorrect login, please try again. If the problem persists please contact the administrator to change the 
		login credentials</div>
		<%
			}
		%>
		<button style="margin-top: 10px;" class="btn btn-lg btn-primary btn-block" type="submit">Sign
			in</button>
	</form>

	
</body>
</html>