<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.Product" %>
<%@ page import="data.DataProduct" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Stock Management App</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>



<%

	DataProduct data = new DataProduct();
	List<Product> products = data.findAll();
	System.out.println("ALL PRODUCTS: " + products);
	Product prod = null;
	for(Product p : products){
		if(p.getPid().equals(Integer.valueOf(request.getParameter("id")))){
			prod = p;
			break;
		}
	}
	System.out.println("PRODUCT IS: " + prod);
%>

<h2 style="text-align: center; margin-top: 10%;">Editing product: <%= prod.getProductName() %></h2>
<div class="container">
	<form action="UpdateProductServlet" method="post" style="margin-top: 200px;">
	

    <!-- <label for="np-id">Product ID</label> -->
	<input type="hidden" id="np-id" value="<%= prod.getPid() %>" type="number" name="np-id" placeholder="Product Id">
	
	<label for="np-name">Product Name:</label>
	<input name="np-name" placeholder="Product Name" value="<%= prod.getProductName() %>" id="np-name">
	
	<label for="np-no_stock">No stock:</label>
	<input  type="number" name="np-no_stock" value="<%= prod.getNoStock() %>" id= "np-no_stock" placeholder="No stock">
	
	<label for="np-image">Product image:</label>
	 <input name="np-image" id="np-image" placeholder="Product image" value="<%= prod.getProductImage() %>">
	 
	 
	  
	 <label for="np-price">Price &euro; :</label>
	 <input  type="number" step="any" name="np-price" id= "np-price" placeholder="Price" value="<%= prod.getPrice() %>">
	 
	 <div style="text-align: center; margin-top: 30px;">
	<input id="sub" type="submit" style="min-width: 400px; " value="Update product">
	</div>
</form>
</div>

<script>
let idsOfNotNegativeAndNotFloatingPointInputs = ['np-id', 'np-no_stock'];

let idsOfNotNegativeInputs = [ 'np-price'];
for(let id of idsOfNotNegativeInputs){
	document.getElementById(id).addEventListener('keyup', function(){
		let input = this.value;
		if(input < 1){
			this.value = '';
			// this.value.replaceAll('-', '');
		}
	})
	
}
for(let id of idsOfNotNegativeAndNotFloatingPointInputs){
	document.getElementById(id).addEventListener('keyup', function(){
		if(this.value.indexOf('.') != -1){
			this.value = '';
			return;
		}
		let input = this.value;
		console.log('input: ', input);
		if(input < 1){
			this.value = '';
		}
	})
	
}
</script>

</body>
</html>