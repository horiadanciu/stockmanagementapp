<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.Product" %>
<%@ page import="model.User" %>
<%@ page import="data.DataProduct" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.math.RoundingMode" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.Collections" %>


<%
	User user = (User)session.getAttribute("USER_LOGGED_IN");
	if(user != null){
		// user is logged in, can use this page
	}else{
		response.sendRedirect("index.jsp");
		return;
	}
%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Stock Management App</title>
<style>
/* Style inputs with type="text", select elements and textareas */
input[type=text], select, textarea {
  width: 100%; /* Full width */
  padding: 12px; /* Some padding */ 
  border: 1px solid #ccc; /* Gray border */
  border-radius: 4px; /* Rounded borders */
  box-sizing: border-box; /* Make sure that padding and width stays in place */
  margin-top: 6px; /* Add a top margin */
  margin-bottom: 16px; /* Bottom margin */
  resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */
}

/* Style the submit button with a specific background color etc */
input#sub {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

/* When moving the mouse over the submit button, add a darker green color */
input[type=submit]:hover {
  background-color: #45a049;
}


</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>

<h2 style=" margin-top: 3%;">List of products</h2>
Welcome, <%= user.getAccessName() %><br>
<a href="LogoutServlet">Logout</a>
<ul style="position: fixed; top: 40%;">
	<li>
		<a href="products.jsp?category=Table">Tables</a>
	</li>
	<li>
		<a href="products.jsp?category=Chair">Chairs</a>
	</li>
	<li>
		<a href="products.jsp?category=Lamp">Lamps</a>
	</li>
	
	<li>
		<a href="products.jsp?category=Desk">Desks</a>
	</li>
	
	<li>
		<a href="products.jsp?category=Sofa">Sofas</a>
	</li>
	<li>
		<a href="products.jsp?category=Bed">Beds</a>
	</li>	
	<li>
		<a href="products.jsp?category=Drawer">Drawers</a>
	</li>	
	<li>
		<a href="products.jsp?category=Wardrobe">Wardrobes</a>
	</li>	

</ul>

<%
	
	DataProduct dataProduct = new DataProduct(); 
	List<Product> products = dataProduct.findAll();
	
	String priceOrder = request.getParameter("price-order");
	if(priceOrder != null){
		if(priceOrder.equals("ASC")){
			Collections.sort(products, (p1, p2) -> (int)(p1.getPrice() - p2.getPrice()));
		}else if(priceOrder.equals("DESC")){
			Collections.sort(products, (p1, p2) -> (int)(p2.getPrice() - p1.getPrice()));
		}
	}
	
	String inStock = request.getParameter("instock");
	System.out.println("IN STOCK: " + inStock);
	if(inStock != null && inStock.equals("on")){
		products = products.stream().filter(p -> p.getNoStock() != 0).collect(Collectors.toList());
	}
	
	String category = request.getParameter("category");
	if(category != null){
		products = products.stream().filter(p -> p.getProductName().contains(category) ).collect(Collectors.toList());
	}
	
	String searchTerm = request.getParameter("search");
	if(searchTerm != null){
		
		if(!searchTerm.isEmpty()){
			products = products.stream().filter(p -> p.getProductName().toLowerCase().contains(searchTerm.toLowerCase()) || (""+p.getPid()).contains(searchTerm.toLowerCase())).collect(Collectors.toList());	
		}
	}
	
	String filterCategory = request.getParameter("filter-category");
	if(filterCategory != null && (!filterCategory.isEmpty())){
		products = products.stream().filter(p -> p.getProductName().toLowerCase().contains(filterCategory.toLowerCase()) ).collect(Collectors.toList());	
	}
%>



<div class="container" style="margin-top: 30px;">





<form>
	Search: <input name="search">
	<input type="submit">
</form>
<a href="products.jsp">Return to list of products</a>


<%
	if(request.getParameter("error") != null){
		if(request.getParameter("error").equals("product-already-exists")){
			%>
<div style="color: red;">
	You have tried to insert a product with an already existing ID!
</div>
			
			<%
		}else{
%>
<div style="color: red;">
	Product id cannot be longer than 4 digits
</div>

<%
		}
	}
%>

<div style="text-align: center;">
<%
	if(products.size() == 0){
%>
	<div style="color: blue;">
		No products matching the search criteria were found!
	</div>
<%
	}else{
%>

<form style="margin-top: 20px; margin-bottom: 20px;">
	<div class="row">
		<div class="col">
			<select name="price-order">
				<option value="">Price - select order</option>
				<option value="ASC">Price - ascending</option>
				<option value="DESC">Price - descending</option>
			</select>
			<br>
			In stock: <input type="checkbox" name="instock">	
		</div>
		<div class="col">
			<select name="filter-category">
				<option value="">Filter by category</option>
				<option value="Table">Tables</option>
				<option value="Chair">Chairs</option>
				<option value="Lamp">Lamps</option>
				<option value="Desk">Desks</option>
				<option value="Sofa">Sofas</option>
				<option value="Bed">Beds</option>
				<option value="Drawer">Drawers</option>
				<option value="Wardrobe">Wardrobes</option>

			</select>
		</div>
		<div class="col">
			<input style="width: 80%;" class="btn btn-info" type="submit" value="Filter"> 
		</div>
		
	</div>

</form>

<table class="table table-bordered" style="margin-bottom: 30px;">
	<thead>
		<tr>
			<th>PID</th>
			<th>Name</th>
			<th>Stock</th>
			<th>Image</th>
			<th>
				Price
			</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		
	<%
	DecimalFormat df2 = new DecimalFormat("#.##");
	for(Product p : products){
		%>
	<tr>
		<td><%= p.getPid() %></td>
		<td><%= p.getProductName() %></td>
		<td><%= p.getNoStock() %></td>
		<td><img style="max-width: 170px; max-height: 170px;" src="<%= p.getProductImage() %>"></td>
		<td style="white-space: nowrap;">

			&euro; <%= String.format("%.2f", p.getPrice()) %>
		</td>
		<td>
			<a href="DeleteProductServlet?id-to-delete=<%= p.getPid() %>">Delete</a>
			<a href="product-edit.jsp?id=<%= p.getPid() %>">Edit</a>
		</td>
	</tr>
		<%
	}
	%>
	
	</tbody>
</table>
<%
	}
%>
</div>

<script>
	// confirm user submit
	function validate(form) {
       return confirm('Are you sure that the inputted information is correct?'); 
	}
</script>

<form action="AddProductServlet" method="post"  onsubmit="return validate(this)">
	

    <label for="np-id">PID</label>
	<input id="np-id" type="number" name="np-id" placeholder="Product Id">
	
	<label for="np-name">Product Name</label>
	<input name="np-name" placeholder="Product Name" id="np-name">
	
	<label for="np-no_stock">No stock:</label>
	<input  type="number" name="np-no_stock" id= "np-no_stock" placeholder="No stock">
	
	<label for="np-image">Product image:</label>
	 <input name="np-image" id="np-image" placeholder="Product image">
	 
	 <label for="np-price">Price &euro; :</label>
	 <input  type="number" step="any" name="np-price" id= "np-price" placeholder="Price">
	 
	 <div style="text-align: center; margin-top: 30px;">
	<input id="sub" type="submit" style="min-width: 400px; " value="Add product">
	</div>
</form>

<script>

	let idsOfNotNegativeAndNotFloatingPointInputs = ['np-id', 'np-no_stock'];

	let idsOfNotNegativeInputs = [ 'np-price'];
	for(let id of idsOfNotNegativeInputs){
		document.getElementById(id).addEventListener('keyup', function(){
			let input = this.value;
			console.log('input: ', input);
			if(input < 1){
				this.value = '';
			}
		})
		
	}
	for(let id of idsOfNotNegativeAndNotFloatingPointInputs){
		document.getElementById(id).addEventListener('keyup', function(){
			if(this.value.indexOf('.') != -1){
				this.value = '';
				return;
			}
			let input = this.value;
			console.log('input: ', input);
			if(input < 1){
				this.value = '';
			}
		})
		
	}

</script>

</div>

</body>
</html>