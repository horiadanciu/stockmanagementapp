package data;

import java.sql.SQLException;
import java.util.List;

public interface IData<T> {

	public List<T> findAll() throws SQLException; // read
	public void insert(T newElement) throws SQLException; // create
	public void delete(int id) throws SQLException; // delete
	
}
