package data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import model.Product;
import model.User;

public class DataUser implements IData<User>{

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
	}
	
	
	// register
	public void insert(User user) throws SQLException {
		Connection con = DbConnectionService.getConnection();
		Statement stmt = con.createStatement();
		
		
		String sqlCommand = "insert into users(access_name, password) values ('"+user.getAccessName()+"', '"+user.getPassword()+"')";
		
		stmt.executeUpdate(sqlCommand);
	}
	
	// login
	public User login(String an, String pwd) throws SQLException {
		Connection con = DbConnectionService.getConnection();
		
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from users where access_name = '"+an+"' AND password ='"+pwd+"'");
		
		User user = null;
		while(rs.next()) {
			user = new User();
			user.setAccessName(rs.getString("access_name"));
			user.setPassword(rs.getString("password"));
		}
		
		return user;
	}

	@Override
	public List<User> findAll() throws SQLException {
		throw new UnsupportedOperationException();
	}

	

	@Override
	public void delete(int id) throws SQLException {
		throw new UnsupportedOperationException();
		
	}
	
	
}
