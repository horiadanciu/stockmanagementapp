package data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Product;

public class DataProduct implements IData<Product> {

	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection con = DbConnectionService.getConnection();
		Statement stmt = con.createStatement();
		
		stmt.executeUpdate("delete from products where pid = " + id);
		
	}
	
	
	// insert into products values ...
	public void insert(Product newProduct) throws SQLException {
		
		Connection con = DbConnectionService.getConnection();
		Statement stmt = con.createStatement();
		
		String image = newProduct.getProductImage();
		if(image == null) {
			image = "https://www.elasticpath.com/sites/default/files/getelastic/open-box.jpg";
		}
		String sqlCommand = "insert into products(pid, product_name, no_stock, product_image, price) values ("+newProduct.getPid()+", '"+newProduct.getProductName()+"', "+newProduct.getNoStock()+", '"+image+"', "+newProduct.getPrice()+")";
		stmt.executeUpdate(sqlCommand);
		
	}
	
	
	// update
	public void update(Product product) throws SQLException {
		Connection con = DbConnectionService.getConnection();
		Statement stmt = con.createStatement();
		stmt.executeUpdate("UPDATE products SET product_name = '"+product.getProductName()+"', no_stock = "+product.getNoStock()+", product_image = '"+product.getProductImage()+"', price = "+product.getPrice()+" WHERE pid = " + product.getPid());
	}
	
	// select * from products
	public List<Product> findAll() throws SQLException{
		
		List<Product> products = new ArrayList<>();
		Connection con = DbConnectionService.getConnection();
		
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from products");
		while(rs.next()) {
			Product product = new Product();
			product.setPid(rs.getInt("pid"));
			product.setNoStock(rs.getInt("no_stock"));
			product.setProductImage(rs.getString("product_image"));
			product.setProductName(rs.getString("product_name"));
			product.setPrice(rs.getDouble("price"));
			
			products.add(product);
		}
		
		return products;
		
	}



	
}
