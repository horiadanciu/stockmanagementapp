package model;

public class Product {

	private Integer pid;
	private String productName;
	private String productImage;
	private Integer noStock;
	private Double price;
	
	
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductImage() {
		return productImage;
	}
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	public Integer getNoStock() {
		return noStock;
	}
	public void setNoStock(Integer noStock) {
		this.noStock = noStock;
	}
	

	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [pid=" + pid + ", productName=" + productName + ", productImage=" + productImage + ", noStock="
				+ noStock + "]";
	}
	
	
	
}
