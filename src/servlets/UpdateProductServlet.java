package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.DataProduct;
import model.Product;

@WebServlet("/UpdateProductServlet")
public class UpdateProductServlet extends HttpServlet {


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("UPDATING");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.valueOf(request.getParameter("np-id"));
		String name = request.getParameter("np-name");
		String image = request.getParameter("np-image");
		Integer noStock = Integer.valueOf(request.getParameter("np-no_stock"));
		Double price = Double.valueOf(request.getParameter("np-price"));
		
		Product theNewProduct = new Product();
		theNewProduct.setPrice(price);
		theNewProduct.setNoStock(noStock);
		theNewProduct.setPid(id);
		if(image.isEmpty()) {
			image = null;
		}
		theNewProduct.setProductImage(image);
		theNewProduct.setProductName(name);
		
		DataProduct data = new DataProduct();
		try {
			data.update(theNewProduct);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect("products.jsp");
	}

}
