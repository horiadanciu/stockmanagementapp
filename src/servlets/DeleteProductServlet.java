package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.DataProduct;


@WebServlet("/DeleteProductServlet")
public class DeleteProductServlet extends HttpServlet {

	private DataProduct dataProduct = new DataProduct();

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		Integer idOfProductToDelete = Integer.valueOf(request.getParameter("id-to-delete"));
		try {
			dataProduct.delete(idOfProductToDelete);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.sendRedirect("products.jsp");
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
