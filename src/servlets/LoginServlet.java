package servlets;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import data.DataUser;
import model.User;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	private DataUser dataUser = new DataUser();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String an = request.getParameter("access-name");
		String pwd = request.getParameter("password");
		
		User u = null;
		try {
			u = dataUser.login(an, pwd);
		}catch(Exception e) {
			e.printStackTrace();
		}
		if(u == null) {
			// login failed
			response.sendRedirect("index.jsp?error=true");
			
		}else {
			// login successful
			HttpSession session = request.getSession(true);
			session.setAttribute("USER_LOGGED_IN", u);
			response.sendRedirect("products.jsp");
		}
	}

}
