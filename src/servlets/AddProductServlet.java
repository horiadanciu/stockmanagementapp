package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.DataProduct;
import model.Product;


@WebServlet("/AddProductServlet")
public class AddProductServlet extends HttpServlet {
	
	
	
	private DataProduct dataProduct = new DataProduct();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Integer id = Integer.valueOf(request.getParameter("np-id"));
		if(id > 9999) {
			response.sendRedirect("products.jsp?error=id");
			return;
		}
		String name = request.getParameter("np-name");
		String image = request.getParameter("np-image");
		Integer noStock = Integer.valueOf(request.getParameter("np-no_stock"));
		
		Product theNewProduct = new Product();
		theNewProduct.setNoStock(noStock);
		theNewProduct.setPid(id);
		if(image.isEmpty()) {
			image = null;
		}
		theNewProduct.setProductImage(image);
		theNewProduct.setProductName(name);
		theNewProduct.setPrice(Double.valueOf(request.getParameter("np-price")));
		
		
		try {
			dataProduct.insert(theNewProduct);
		} catch (SQLException e) {
			e.printStackTrace();
			response.sendRedirect("products.jsp?error=product-already-exists");
			return;
		}
		
		response.sendRedirect("products.jsp");
		
	}

}
